# Webhook

Before you can run your tests you will need to add a `Webhook` java file to your *test project*. This can be done in the directory `src/test/java/{package name}/hooks` at the same level as your `features`, `steps` etc. directories.
The Webhook has 2 purposes:
1. To configure an [application context](#application context).
2. To add [setup and teardown methods](#setup) for each scenario.

An example of a `Webhook` file can be:
```java
import com.capgemini.SpringConfiguration;
import com.capgemini.webdriver.Webdriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = SpringConfiguration.class, loader = SpringBootContextLoader.class)
public class Webhook {

    @Autowired
    private Webdriver driver;

    @Before
    public void setup() {
        // prepare driver, e.g. set screen size.
    }

    @After
    public void teardown(final Scenario scenario) {
        driver.createScreenshotOnFail(scenario);
        driver.close();
        driver.quit();
    }
}
```

## <a name="application context"></a>Application Context Configuration

To be able to make use of the necessary Spring Boot functionality, such as dependency injection, you need to configure the Spring context. The `@ContextConfiguration` annotation is given two options:
- the `classes = SpringConfiguration.class` option refers to the SpringConfiguration class in the *framework*, which takes care of enabling autoconfiguration and component scanning.
- the `loader = SpringBootContextLoader.class` option makes sure the Spring Boot context is loaded at the beginning of the test run, which enables externalized configuration. Without externalized configuration, the application.yml files will not be processed.

## <a name="setup"></a>Setup and Teardown methods

In the example we have chosen for the names `setup` and `teardown` for our methods. However, you are free to choose any name you like for these methods. The `@Before` hook is how the application determines that this method has to be run before each scenario. Just as the method under the `@After` hook will be run after each scenario. 

The before method can be used to setup operations that you would like to do before each scenario. Some examples:
- Delete all cookies
- set your browser screen size
- etc

And in the same way the after method can be used for teardown operations after each scenario. In the example code above a screenshot is created if the scenario has failed. And after each scenario the browser is closed. 

At the moment, the current version of Cucumber JVM does not support a `@BeforeAll` or `@AfterAll` hook. This issue has been reported and there are some workarounds available in [issue 515](https://github.com/cucumber/cucumber-jvm/issues/515).

