package com.capgemini.webdriver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ChromeConfigTest {

    private ChromeConfig chromeConfig;

    private static final boolean EXPECTED_HEADLESS = true;
    private static final List<String> EXPECTED_ARGUMENTS = Arrays.asList("argument 1", "argument 2");

    @BeforeEach
    public void setUp() {
        // Given the chromeConfig properties are set
        chromeConfig = new ChromeConfig();
        chromeConfig.setHeadless(EXPECTED_HEADLESS);
        chromeConfig.setArguments(EXPECTED_ARGUMENTS);
    }

    @Test
    public void getOptions() {
        // When getOptions is called
        ChromeOptions chromeOptions = chromeConfig.getOptions();

        // Then the retrieved options contain the property values
        assertNotNull(chromeOptions);
        String actualArguments = chromeOptions.asMap().get("goog:chromeOptions").toString();
        assertThat(actualArguments, allOf(
                containsString("--headless"),
                containsString("--disable-gpu"),
                containsString(EXPECTED_ARGUMENTS.get(0)),
                containsString(EXPECTED_ARGUMENTS.get(1))
        ));
    }
}