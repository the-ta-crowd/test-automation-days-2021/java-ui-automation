package com.capgemini.webdriver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class FirefoxConfigTest {

    private FirefoxConfig firefoxConfig;

    private static final boolean EXPECTED_HEADLESS = true;
    private static final Map<String, String> EXPECTED_PREFERENCES;

    static {
        EXPECTED_PREFERENCES = new HashMap<>();
        EXPECTED_PREFERENCES.put("argument 1", "argument 1");
        EXPECTED_PREFERENCES.put("argument 2", "argument 2");
    }

    @BeforeEach
    void setUp() {
        // Given the firefoxConfig properties are set
        firefoxConfig = new FirefoxConfig();
        firefoxConfig.setHeadless(EXPECTED_HEADLESS);
        firefoxConfig.setPreferences(EXPECTED_PREFERENCES);
    }

    @Test
    void getOptions() {
        // When getOptions is called
        FirefoxOptions firefoxOptions = firefoxConfig.getOptions();

        // Then the retrieved options contain the property values
        assertNotNull(firefoxOptions);
        String actualPreferences = firefoxOptions.asMap().get("moz:firefoxOptions").toString();
        assertThat(actualPreferences, containsString("-headless"));
    }
}