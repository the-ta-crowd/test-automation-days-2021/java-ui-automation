package com.capgemini.webdriver;

import io.cucumber.java.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import lombok.Getter;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * The Webdriver class allows you to drive a browser just like a user would, meaning you can perform actions such
 * as navigate to a URL, click on page elements and resize your browser window.
 *
 * Our implementation of the webdriver delegates most of these functions to the underlying Selenium Webdriver, see
 * {@link org.openqa.selenium.WebDriver} for a description of its methods. For the custom functionality, our webdriver
 * makes use of Spring Boot to enable you to:
 * - set a base-URL per environment, e.g. development/test/staging.
 * - choose a browser type, e.g. chrome or firefox, per test run.
 * - easily configure browser options in a yml file.
 */
@Component
public class Webdriver {

    /**
     * The {@link org.openqa.selenium.support.events.EventFiringWebDriver} extends the Selenium Webdriver with multiple
     * useful functions, such as taking screenshots and executing JavaScript.
     */
    private EventFiringWebDriver webDriver;

    @Value("${baseUrl}")
    @Getter
    private String baseUrl;
    @Value("${browserType:chrome}")
    @Getter
    private String browserType;

    @Autowired
    private ChromeConfig chromeConfig;
    @Autowired
    private FirefoxConfig firefoxConfig;

    /**
     * The browserType attribute is initialized by Spring Boot at runtime from an application.yml file in your integration
     * test project. It defaults to chrome if no value is set, or throws an exception if no application.yml file is found.
     *
     * For initializing a webdriver instance, the {@link io.github.bonigarcia.wdm.WebDriverManager} is used. This
     * class automatically handles the management of the binary drivers (e.g. chromedriver, geckodriver) for you.
     */
    @PostConstruct
    public void initWebdriver() {
        switch (browserType) {
            case "chrome":
                WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
                webDriver = new EventFiringWebDriver(new ChromeDriver(chromeConfig.getOptions()));
                break;
            case "firefox":
                WebDriverManager.getInstance(DriverManagerType.FIREFOX).setup();
                webDriver = new EventFiringWebDriver(new FirefoxDriver(firefoxConfig.getOptions()));
                break;
            default:
                throw new UnsupportedOperationException(browserType + " is not recognized as supported browser type");
        }
    }

    /**
     * This method is used to navigate to a webpage. It uses the baseUrl attribute, which is initialized by Spring Boot
     * at runtime from an application.yml file in your integration test project. This will throw an exception if no
     * application.yml file is found.
     * @param uri   The URI to append to the baseUrl, e.g. "/home"
     */
    public void get(String uri) {
        webDriver.get(baseUrl + uri);
    }

    /**
     * Performs a screenshot of the webpage in case the test scenario fails.
     * @param scenario  Class containing details on the current test scenario, such as scenario name and failed/passed status.
     *                  Can only be used in a Cucumber @Before or @After method.
     */
    public void createScreenshotOnFail(final Scenario scenario) {
        if (scenario.isFailed()) {
            scenario.write("Test failed at url: " + webDriver.getCurrentUrl());
            scenario.embed(webDriver.getScreenshotAs(OutputType.BYTES), "image/png", "failed_state");
        }
    }

    public EventFiringWebDriver register(WebDriverEventListener eventListener) {
        return webDriver.register(eventListener);
    }

    public EventFiringWebDriver unregister(WebDriverEventListener eventListener) {
        return webDriver.unregister(eventListener);
    }

    public WebDriver getWrappedDriver() {
        return webDriver.getWrappedDriver();
    }

    public String getCurrentUrl() {
        return webDriver.getCurrentUrl();
    }

    public String getTitle() {
        return webDriver.getTitle();
    }

    public List<WebElement> findElements(By by) {
        return webDriver.findElements(by);
    }

    public WebElement findElement(By by) {
        return webDriver.findElement(by);
    }

    public String getPageSource() {
        return webDriver.getPageSource();
    }

    public void close() {
        webDriver.close();
    }

    public void quit() {
        webDriver.quit();
    }

    public Set<String> getWindowHandles() {
        return webDriver.getWindowHandles();
    }

    public String getWindowHandle() {
        return webDriver.getWindowHandle();
    }

    public Object executeScript(String script, Object... args) {
        return webDriver.executeScript(script, args);
    }

    public Object executeAsyncScript(String script, Object... args) {
        return webDriver.executeAsyncScript(script, args);
    }

    public <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException {
        return webDriver.getScreenshotAs(target);
    }

    public WebDriver.TargetLocator switchTo() {
        return webDriver.switchTo();
    }

    public WebDriver.Navigation navigate() {
        return webDriver.navigate();
    }

    public WebDriver.Options manage() {
        return webDriver.manage();
    }

    public void perform(Collection<Sequence> actions) {
        webDriver.perform(actions);
    }

    public void resetInputState() {
        webDriver.resetInputState();
    }

    public Capabilities getCapabilities() {
        return webDriver.getCapabilities();
    }
}
