package com.capgemini.steps.common;

import io.cucumber.java.en.Given;

public class CommonSteps extends BaseSteps {

    @Given("an admin user is logged in")
    public void theUserLogsInAsAdmin() {
        pages.common.navigateTo("/users/login");
        pages.login.loginAsAdmin();
    }
}
