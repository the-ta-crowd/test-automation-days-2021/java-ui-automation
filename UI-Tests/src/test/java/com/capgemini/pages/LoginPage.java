package com.capgemini.pages;

import com.capgemini.pages.common.BasePage;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

@Component
public class LoginPage extends BasePage {

    private static final String USERNAME_SELECTOR = "input[name='username']";
    private static final String PASSWORD_SELECTOR = "input[name='password']";
    private static final String SUBMIT_BUTTON_SELECTOR = "button[value='Submit']";

    public void loginAsAdmin() {
        driver.findElement(By.cssSelector(USERNAME_SELECTOR)).sendKeys("admin");
        driver.findElement(By.cssSelector(PASSWORD_SELECTOR)).sendKeys("admin");
        driver.findElement(By.cssSelector(SUBMIT_BUTTON_SELECTOR)).click();
    }
}
