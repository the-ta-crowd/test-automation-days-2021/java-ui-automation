package com.capgemini.pages.common;

import com.capgemini.pages.LoginPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Pages {

    @Autowired
    public CommonPage common;
    @Autowired
    public LoginPage login;
}
