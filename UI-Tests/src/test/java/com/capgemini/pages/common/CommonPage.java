package com.capgemini.pages.common;

import org.springframework.stereotype.Component;

@Component
public class CommonPage extends BasePage {

    public void navigateTo(final String uri) {
        driver.get(uri);
    }

}
