package com.capgemini.hooks;

import com.capgemini.SpringConfiguration;
import com.capgemini.webdriver.Webdriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = SpringConfiguration.class, loader = SpringBootContextLoader.class)
public class Webhook {

    @Autowired
    private Webdriver driver;

    @Before
    public void setup() {
        // prepare driver, e.g. set screen size.
        driver.manage().window().maximize();
    }

    @After
    public void teardown(final Scenario scenario) {
        driver.createScreenshotOnFail(scenario);
        driver.close();
        driver.quit();
    }
}
